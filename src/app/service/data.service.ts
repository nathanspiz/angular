import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { API_URL, APP_CONFIG, SaleAd } from '../resources';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  constructor(private http: HttpClient) { }

  private titleSource = new BehaviorSubject('Default Title')
  currentTitle = this.titleSource.asObservable();

  changeTitle(title:string)
  {
    this.titleSource.next(title)
  }

  getSaleAds(){
    let headers = new HttpHeaders();
    headers = headers.set('Accept', 'application/json')
      .set('Authorization', 'Bearer ' + localStorage.getItem(APP_CONFIG.token))
      .set('Content-type', 'application/json')
    return this.http.get(API_URL.saleAds, { headers : headers })
  }

  getSaleAd(id : number){
    let headers = new HttpHeaders();
    headers = headers.set('Accept', 'application/json')
      .set('Authorization', 'Bearer ' + localStorage.getItem(APP_CONFIG.token))
      .set('Content-type', 'application/json')
    return this.http.get(API_URL.saleAd + '/' + id, { headers : headers })
  }

  deleteSaleAd(id : number){
    let headers = new HttpHeaders();
    headers = headers.set('Accept', 'application/json')
      .set('Authorization', 'Bearer ' + localStorage.getItem(APP_CONFIG.token))
      .set('Content-type', 'application/json')
    return this.http.delete(API_URL.saleAd + '/' + id,{ headers : headers})
  }

  updateSaleAd(saleAd: SaleAd){
    let headers = new HttpHeaders();
    headers = headers.set('Accept', 'application/json')
      .set('Authorization', 'Bearer ' + localStorage.getItem(APP_CONFIG.token))
      .set('Content-type', 'application/json')
    return this.http.put(API_URL.saleAd + '/' + saleAd.id, saleAd,{ headers : headers})

  }

  updateSaleAdWithFile(saleAd : FormData){
    let headers = new HttpHeaders();
    headers = headers.set('Accept', 'application/json')
      .set('Authorization', 'Bearer ' + localStorage.getItem(APP_CONFIG.token))
    return this.http.post(API_URL.saleAds, saleAd,{ headers : headers})

  }
}
