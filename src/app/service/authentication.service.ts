import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { API_URL, APP_CONFIG } from '../resources';
import { Observable, Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  constructor(private http : HttpClient) { }

  isAuthenticated = new Subject();
  isAuthenticatedObservable = this.isAuthenticated.asObservable();

  login(username: string | undefined, password: string | undefined) : Observable<Boolean>
  {
    // Login est valide
    // Enregistrer en session (localStorage) le token de l'utilisateur
    // On informe l'appellant que l'utilisateur est authentifié et qu'il faut le rediriger sur la page appropriée
    // Login n'est pas valide
    // Informer l'appellant que l'utilisateur n'est pas authentifié

    return new Observable(observer => {
      this.doLogin(username, password).subscribe(
        (response)=>{
          if(response.access_token){
            let token = response.access_token;
            localStorage.setItem(APP_CONFIG.token, token);
            this.isAuthenticated.next(true);
            observer.next(true);
          }else{
            // this.authenticated.next(false)
            observer.next(false)
          }
        },
        ()=>{
          observer.next(false);
        }
      )
    })
  }

  doLogin(username: string | undefined, password: string | undefined) : Observable<any>
  {
    // Fait l'appel au service distant
    // Va récupérer le token si login valide
    // Retourner l'info à l'appellant (login)

    let bodyRequest = {
      "username" : username,
      "password" : password
    }

    let headers = new HttpHeaders();
    headers = headers.set('Content-Type', 'application/json; charset=utf-8')
                     .set('Accept', 'application/json');

    return this.http.post(API_URL.login, bodyRequest,{ headers : headers })
  }

  isUserLoggedIn()
  {
    // Récupérer le token dans le session storage (s'il y en a un)
    // Si token, alors on considère l'utilisateur comme étant authentifié
    // Sinon non

    if(localStorage.getItem(APP_CONFIG.token) != null){
      return this.isAuthenticated.next(true);
    }else{
      return this.isAuthenticated.next(false);
    }
  }

  logout()
  {
    // On efface le token dans le session storage
    // On confirme le logout au composant appellant qui devra rediriger l'utilisateur sur la page appropriée

    localStorage.removeItem(APP_CONFIG.token)
    this.isAuthenticated.next(false)
    return true
  }
}
