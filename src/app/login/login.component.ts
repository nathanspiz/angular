import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { DataService } from '../service/data.service';
import { AuthenticationService } from '../service/authentication.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  username: string | undefined
  password: string | undefined
  message = "Invalid Credentials"
  success = false

  constructor(
    private data: DataService,
    private titleService: Title,
    private router: Router,
    private authentication: AuthenticationService
  ) { }

  ngOnInit(): void {
    this.data.changeTitle("Sign in");
    this.titleService.setTitle("Login page");
    //Rediriger l'utilisateur vers liste si celui ci est connecté
    if(sessionStorage.getItem('token')){
      this.router.navigate(['list'])
    }

  }

  handleLogin() {
    this.authentication.login(this.username, this.password).subscribe(
      response =>{
        this.success = false
        if (response)
          this.success = false;
        else this.success = true;

        if (this.success) {
          console.log("Failed !!")
        }
        else {
          this.router.navigate(['list'])
        }
      },
      error =>{
        console.log(error);
      }
    )
  }
}
