import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { DataService } from '../service/data.service';
import {SaleAd} from '../resources'

@Component({
  selector: 'app-sale-ad-list',
  templateUrl: './sale-ad-list.component.html',
  styleUrls: ['./sale-ad-list.component.scss']
})
export class SaleAdListComponent implements OnInit {

  saleAdList : any = [SaleAd]
  constructor(
    private data: DataService,
    private titleService: Title,
  ) { }

  ngOnInit(): void {
    this.data.changeTitle("Sale Ad List")
    this.titleService.setTitle("Sale Ad List Page")

    this.onGetSaleAds()
  }

  onGetSaleAds(){
    this.data.getSaleAds().subscribe(
      (response)=>{
        this.saleAdList = response;
        console.log(response);
      },
      (error)=>{
        console.log(error);
      }

    )
  }

  handleDeleteSuccess(response: any){
    if(response){
      this.onGetSaleAds();
    }
  }
}
