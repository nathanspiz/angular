export const API_URL = {
  login : 'http://localhost:8080/api/login',
  saleAds : 'http://localhost:8080/api/saleAds',
  saleAd : 'http://localhost:8080/api/saleAd',
}

export const APP_CONFIG = {
  token: 'token',
  imageDirectoryPath : '/home/nathan/Code/Lecoincoin_anne_final/grails-app/assets/images/'
}

export class SaleAd{
  id!: number;
  title!: string;
  description!: string;
  dateCreated!: string;
  price!: number;
  illustrations!: [string]
}
